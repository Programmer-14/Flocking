/* Credit: This is based on Daniel Shiffman's Nature of Code tutorial - Autonous Agent.
Autonomous Agent - limited ability to percieve it's environment.

Flocking:
- Action/Section
- Steering -> Steering = desire - velocity
- Locomotion
*/

// Setting up flowfield object and "Ball" array list;---------------------------
Flowfield flowfield;
ArrayList<Ball> b;

// Canvas setup ----------------------------------------------------------------
void setup(){
  size(800, 600);
  flowfield = new Flowfield(20);
  b = new ArrayList<Ball>();

  for (int i = 0; i < 300; i++){
    b.add(new Ball(new PVector(random(width), random(height)), random(2,5), random(0.1, 0.5)));
  }

}

void draw(){
  background(#3bbdfc); // Light Blue colour.
  flowfield.update();

  for (Ball ball : b){
    ball.follow(flowfield);
    ball.run();
  }

  fill(270); // A white colour.
}

// Changes the path of the triangle object when the mouse is pressed.
void mousePressed(){
  flowfield.init();
}
