class Ball{

  // Initial class variable Objects
  PVector position;
  PVector velocity;
  PVector acceleration;

  // Class variables
  float r;
  float maxforce;
  float maxspeed;

  // Constructor for the class object "Ball".-----------------------------------
  Ball(PVector loc, float maxsp, float maxf){
    acceleration = new PVector(0,0);
    velocity = new PVector(0,0);
    position = loc.get();

    r = 6;

    maxspeed = maxsp;
    maxforce = maxf;
  }

  // Class Methods -------------------------------------------------------------
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxspeed);
    position.add(velocity);
    acceleration.mult(0);
  }

  void applyForce(PVector force){
    // Acceleration = Force / Mass
    acceleration.add(force);
  }


  // void seek(PVector target){
  //   Desired = target - position
  //   PVector desired = PVector.sub(target, position);
  //   desired.setMag(maxspeed);
  //
  //   //Steer = Desired - Velocity
  //   PVector steer = PVector.sub(desired, velocity);
  //   steer.limit(maxforce);
  //
  //   applyForce(steer);
  //
  // }

  void follow(Flowfield flow){
    PVector desired = flow.lookup(position);
    desired.mult(maxspeed);

    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxforce);
    applyForce(steer);
  }

  public void run(){
    update();
    borders();
    display();
  }

  // Creates a border.
  void borders(){
    if(position.x < -r){
      position.x = width + r;
    }
    if(position.y < -r){
      position.y = height + r;
    }
    if(position.x > width + r){
      position.x = -r;
    }
    if(position.y > height + r){
      position.y = -r;
    }
  }

  // Displays the triangle particle thing.--------------------------------------
  void display(){
    float theta = velocity.heading2D() + radians(90);
    fill(#ffffff);
    stroke(#e8e8e8);
    strokeWeight(1);
    pushMatrix();
    translate(position.x,position.y);
    rotate(theta);

    /* Original Ellipse
    Uncomment to see a weird patricle like movement. */
    //ellipse(0,0,r*2,r*2);

    /* Drawing the little triangle.
    I originally started with an ellipse, but change to triangle because they look better.*/
    beginShape();
    vertex(0,-r*2);
    vertex(-r,r*2);
    vertex(r,r*2);
    endShape();

    // Don't forget to comment out the above code if you uncomment the ellipse.
    popMatrix();

  }

}
