class Flowfield {
  
  // 2D array for Flowfield
  PVector[][] field;
  int columns, rows;
  int res;

  float znoise = 0.0;

  Flowfield(int r){
    res = r;
    columns = width/res;
    rows = height/res;
    field = new PVector[columns][rows];
    init();
  }

  // Generates new Flowfield each time.
  void init(){
    noiseSeed((int)random(10000));

    float xoff = 0;
    for (int i = 0; i < columns; i++){
      float yoff = 0;
      for (int e = 0; e < rows; e++){
        float theta = map(noise(xoff, yoff), 0, 1, 0, TWO_PI);
        field[i][e] = new PVector(cos(theta), sin(theta));
        yoff += 0.1;
      }
      xoff += 0.1;
    }
  }

  void update(){
    float xoff = 0;
    for (int i = 0; i < columns; i++){
      float yoff = 0;
      for (int e = 0; e < rows; e++){
        float theta = map(noise(xoff, yoff), 0, 1, 0, TWO_PI);
        field[i][e] = PVector.fromAngle(theta);
        yoff += 0.1;
      }
      xoff += 0.1;
    }
    znoise += 0.1;
  }

  // Creates the flowfield path for the triangle things.
  void drawVector(PVector v, float x, float y, float scayl){
    pushMatrix();
    float arrow = 4;
    translate(x, y);
    stroke(0, 150);
    rotate(v.heading2D());
    float len = v.mag() * scayl;
    line(0, 0, len, 0);
    popMatrix();
  }

  // Displays the flowfield, even though we can't see it in the simulation.
  void display(){
    for (int i = 0; i < columns; i++){
      for (int e = 0; e < rows; e++){
        drawVector(field[i][e], i * res, e * res, res - 2);
      }
    }
  }

  PVector lookup(PVector lookup){
    int column = int(constrain(lookup.x/res, 0, columns - 1));
    int row = int(constrain(lookup.y/res, 0, rows - 1));
    return field[column][row].get();
  }

}
