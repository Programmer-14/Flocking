# Flocking

> Credit :

>This is based on Daniel Shiffman's Nature of Code tutorial - Autonomous Agent.

This project is an attempt to demonstrate flocking using Java and processing.

## How to run it
There are two ways you can see this code in action.
* You can run it in the Processing [application](https://processing.org).
* Or you can run it inside of the Atom text editor with [this](https://atom.io/packages/processing) plugin.

## Contributions
Feel free to enhance the code in anyway you would like.
