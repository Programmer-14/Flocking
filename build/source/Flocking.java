import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Flocking extends PApplet {

/* Credit: This is based on Daniel Shiffman's Nature of Code tutorial - Autonous Agent.
Autonomous Agent - limited ability to percieve it's environment.

Flocking:
- Action/Section
- Steering -> Steering = desire - velocity
- Locomotion
*/

// Setting up flowfield object and "Ball" array list;---------------------------
Flowfield flowfield;
ArrayList<Ball> b;

// Canvas setup ----------------------------------------------------------------
public void setup(){
  
  flowfield = new Flowfield(20);
  b = new ArrayList<Ball>();

  for (int i = 0; i < 300; i++){
    b.add(new Ball(new PVector(random(width), random(height)), random(2,5), random(0.1f, 0.5f)));
  }

}

public void draw(){
  background(0xff3bbdfc); // Light Blue colour.
  flowfield.update();

  for (Ball ball : b){
    ball.follow(flowfield);
    ball.run();
  }

  fill(270); // A white colour.
}

// Changes the path of the triangle object when the mouse is pressed.
public void mousePressed(){
  flowfield.init();
}
class Ball{

  // Initial class variable Objects
  PVector position;
  PVector velocity;
  PVector acceleration;

  // Class variables
  float r;
  float maxforce;
  float maxspeed;

  // Constructor for the class object "Ball".-----------------------------------
  Ball(PVector loc, float maxsp, float maxf){
    acceleration = new PVector(0,0);
    velocity = new PVector(0,0);
    position = loc.get();

    r = 6;

    maxspeed = maxsp;
    maxforce = maxf;
  }

  // Class Methods -------------------------------------------------------------
  public void update(){
    velocity.add(acceleration);
    velocity.limit(maxspeed);
    position.add(velocity);
    acceleration.mult(0);
  }

  public void applyForce(PVector force){
    // Acceleration = Force / Mass
    acceleration.add(force);
  }


  // void seek(PVector target){
  //   Desired = target - position
  //   PVector desired = PVector.sub(target, position);
  //   desired.setMag(maxspeed);
  //
  //   //Steer = Desired - Velocity
  //   PVector steer = PVector.sub(desired, velocity);
  //   steer.limit(maxforce);
  //
  //   applyForce(steer);
  //
  // }

  public void follow(Flowfield flow){
    PVector desired = flow.lookup(position);
    desired.mult(maxspeed);

    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxforce);
    applyForce(steer);
  }

  public void run(){
    update();
    borders();
    display();
  }

  // Creates a border.
  public void borders(){
    if(position.x < -r){
      position.x = width + r;
    }
    if(position.y < -r){
      position.y = height + r;
    }
    if(position.x > width + r){
      position.x = -r;
    }
    if(position.y > height + r){
      position.y = -r;
    }
  }

  // Displays the triangle particle thing.--------------------------------------
  public void display(){
    float theta = velocity.heading2D() + radians(90);
    fill(0xffffffff);
    stroke(0xffe8e8e8);
    strokeWeight(1);
    pushMatrix();
    translate(position.x,position.y);
    rotate(theta);

    /* Original Ellipse
    Uncomment to see a weird patricle like movement. */
    //ellipse(0,0,r*2,r*2);

    /* Drawing the little triangle.
    I originally started with an ellipse, but change to triangle because they look better.*/
    beginShape();
    vertex(0,-r*2);
    vertex(-r,r*2);
    vertex(r,r*2);
    endShape();

    // Don't forget to comment out the above code if you uncomment the ellipse.
    popMatrix();

  }

}
class Flowfield {
  
  // 2D array for Flowfield
  PVector[][] field;
  int columns, rows;
  int res;

  float znoise = 0.0f;

  Flowfield(int r){
    res = r;
    columns = width/res;
    rows = height/res;
    field = new PVector[columns][rows];
    init();
  }

  // Generates new Flowfield each time.
  public void init(){
    noiseSeed((int)random(10000));

    float xoff = 0;
    for (int i = 0; i < columns; i++){
      float yoff = 0;
      for (int e = 0; e < rows; e++){
        float theta = map(noise(xoff, yoff), 0, 1, 0, TWO_PI);
        field[i][e] = new PVector(cos(theta), sin(theta));
        yoff += 0.1f;
      }
      xoff += 0.1f;
    }
  }

  public void update(){
    float xoff = 0;
    for (int i = 0; i < columns; i++){
      float yoff = 0;
      for (int e = 0; e < rows; e++){
        float theta = map(noise(xoff, yoff), 0, 1, 0, TWO_PI);
        field[i][e] = PVector.fromAngle(theta);
        yoff += 0.1f;
      }
      xoff += 0.1f;
    }
    znoise += 0.1f;
  }

  // Creates the flowfield path for the triangle things.
  public void drawVector(PVector v, float x, float y, float scayl){
    pushMatrix();
    float arrow = 4;
    translate(x, y);
    stroke(0, 150);
    rotate(v.heading2D());
    float len = v.mag() * scayl;
    line(0, 0, len, 0);
    popMatrix();
  }

  // Displays the flowfield, even though we can't see it in the simulation.
  public void display(){
    for (int i = 0; i < columns; i++){
      for (int e = 0; e < rows; e++){
        drawVector(field[i][e], i * res, e * res, res - 2);
      }
    }
  }

  public PVector lookup(PVector lookup){
    int column = PApplet.parseInt(constrain(lookup.x/res, 0, columns - 1));
    int row = PApplet.parseInt(constrain(lookup.y/res, 0, rows - 1));
    return field[column][row].get();
  }

}
  public void settings() {  size(800, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Flocking" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
